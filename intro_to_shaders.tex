\documentclass[xetex]{beamer}

\usefonttheme{professionalfonts}
\usefonttheme[onlymath]{serif}

\usetheme{Szeged}
\usecolortheme{dolphin}

\usepackage{listings}
\input{lstGLSL}
\input{lstCPP}

\usepackage{tikz, pgfplots}
\usetikzlibrary{shapes.geometric, arrows}

\tikzstyle{arrow} = [draw]
\tikzstyle{line}= [draw, -latex', thick]

\tikzstyle{block} = %
[
	rectangle,%
	minimum width=2cm,%
	minimum height=1cm,%
	text centered,%
	draw=black,%
	semithick,
	text width=2cm,
]
\tikzstyle{shader} = %
[
	block,%
	dash pattern=on 2pt off 2pt,
]

\tikzstyle{fig} = %
[
	rectangle,%
	minimum width=2cm,%
	minimum height=1cm,%
	semithick,
	text centered,%
	text width=2cm,
]

\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Contents}
    \tableofcontents[currentsection]
  \end{frame}
}

\lstGLSL

\title[Shader Programming]{Shader Programming with Modern OpenGL}
\subtitle{\vspace{20pt}\includegraphics[width=4cm]{OpenGL_logo_(Nov14).svg.png}\\G53GRA Computer Graphics}
\logo{}
\author[W Ward]{Wil Ward\\\href{mailto:wil.ward@nottingham.ac.uk}{\textcolor{blue}{\texttt{wil.ward@nottingham.ac.uk}}}}
\date{6$^\text{th}$ April 2017}

\begin{document}
\frame{\titlepage}

%% SAMPLE FRAME
%\begin{frame}
%	\frametitle{}
%	\begin{columns}[T]
%		\begin{column}[T]{5cm}
%			\begin{itemize}
%				\item .
%			\end{itemize}
%		\end{column}
%		\begin{column}[T]{5cm}
%			
%		\end{column}
%	\end{columns}
%\end{frame}

\section{Introduction}
% FRAME 1 Intro
\begin{frame}
	\frametitle{Introduction}

	\begin{itemize}
		\item Modern OpenGL typically refers to programmable pipeline OpenGL
		\item Shader language allows programming executed on GPU during graphics rendering pipeline
		\item Initial release in 2003 with OpenGL 2.0, but generally OpenGL 3.3 (released 2010) onwards used
		\item Newer versions target hardware advances
		\item Current version: OpenGL 4.5 (released 2014)
	\end{itemize}
	
	\begin{block}{\centering This tutorial is based on OpenGL 3.3}
	\end{block}
\end{frame}

\section{Graphics Pipeline}
% FRAME 2 Recap
\begin{frame}[fragile]
	\frametitle{OpenGL Pipeline}
	\lstCPP
	\begin{columns}[T]
		\begin{column}[T]{5cm}
			\begin{itemize}
				\item So far, programming has used fixed pipeline
				\item Transformations and colouring handled implicitly
				\item \lstinline{glTranslate()}, \lstinline{glColor()}, etc.
			\end{itemize}
		\end{column}\pause
		\begin{column}[T]{5cm}
			\begin{itemize}
				\item Programmable pipeline allows direct control of these
				\item Vertex shader handles transformations, viewing and projection
				\item Fragment shader outputs colour for given fragment
			\end{itemize}
		\end{column}
	\end{columns}
	
\end{frame}

% FRAME 3 Pipeline
\begin{frame}
	\frametitle{OpenGL Pipeline: \only<1>{Fixed}\only<2>{Programmable}}

	\begin{tikzpicture}[node distance=1cm]
%%% NODES	
	\node (data) [block] {\footnotesize Vertex Data};
	
	\uncover<1>%
	{%
		\node (trans) [block, right of=data, xshift=3cm, fill=red!30] {\footnotesize Transformation \& Normals};%
		\node (view) [block, right of=trans, xshift=1.5cm, fill=red!30] {\footnotesize Viewing \& Projection};
	}
	\uncover<2>%
	{%
		\node (vshader) [shader, right of=data, xshift=3cm, fill=red!30] {\footnotesize \bf Vertex Shader};%
	}
	
	\node (clip) [block, below of=trans, yshift=-1cm, fill=green!30] {\footnotesize Clipping};	
	\node (assembly) [block,  left of=clip, xshift=-1.5cm, fill=green!30] {\footnotesize Primitive Assembly};
	\node (raster) [block, right of=clip, xshift=1.5cm, fill=green!30] {\footnotesize Rasterisation};
	
	\uncover<1>%
	{%
		\node (texture) [block, below of=assembly, yshift=-1cm, fill=blue!30] {\footnotesize Texturing};
		\node (colour) [block, right of=texture, xshift=1.5cm, fill=blue!30] {\footnotesize Colour Calculations};
		\node (alpha) [block, right of=colour, xshift=1.5cm, fill=blue!30] {\footnotesize Alpha Testing};%
	}
	\uncover<2>%
	{%
		\node (fshader) [shader, below of=clip, yshift=-1cm,fill=blue!30] {\footnotesize \bf Fragment Shader};
	}
	
	\node (depth) [block, below of=texture, yshift=-1cm, xshift=1.25cm, fill=green!30] {\footnotesize Depth Testing};
	\node (blend) [block, right of=depth, xshift=1.5cm, fill=green!30] {\footnotesize Blending};
	
	\node (frame) [block, right of=blend, xshift=2.5cm] {\footnotesize Frame Buffer};
	
%%% PATHS	
	\path [line] (data) -- (trans);
	\uncover<1>%
	{%
		\path [line] (trans) -- (view);
		\path [line] (view) -- node [near start] {} ([yshift=0.5cm] raster.north) -| (assembly);
	}
	\uncover<2>%
	{%
		\path [line] (vshader) -- node [near start] {} ([yshift=0.5cm] clip.north) -| (assembly);
	}
	\path [line] (assembly) -- (clip);	
	\path [line] (clip) -- (raster);
	\uncover<1>%
	{%
		\path [line] (raster) |- node [near start] {} ([yshift=0.5cm] colour.north) -| (texture.north);
	
		\path [line] (texture) -- (colour);
		\path [line] (colour) -- (alpha);
	
		\path [line] (alpha) |- node [near start] {} ([yshift=0.5cm] blend.north) -| (depth.north);
	}
	\uncover<2>%
	{%
		\path [line] (raster) -- node [near start] {} ([yshift=-0.5cm] raster.south) -| (fshader.north);
		\path [line] (fshader) -- node [near start] {} ([yshift=-0.5cm] fshader.south) -| (depth.north);
	}
	
	\path [line] (depth) -- (blend);
	\path [line] (blend) -- (frame);
	
	\end{tikzpicture}

\end{frame}

\begin{frame}
	\frametitle{OpenGL Pipeline: Visualisation}

	\begin{tikzpicture}
		\node (data) [fig, draw=black] {\def\svgwidth{2cm}\input{svg/vertex.pdf_tex} \footnotesize Vertex Data};
		
		\node (trans) [fig, right of=data, xshift=2cm, draw=red!30, thick,dash pattern=on 2pt off 2pt] {\def\svgwidth{2cm}\input{svg/transformed.pdf_tex} \footnotesize Vertex Shading};
		
		\node (prim) [fig, right of=trans, xshift=2cm,draw=green!30] {\def \svgwidth{2cm}\input{svg/primitive.pdf_tex} \footnotesize Primitive Assembly};
				
		\node (raster) [fig, right of=prim, xshift=2cm,draw=green!30] {\def \svgwidth{2cm}\input{svg/raster.pdf_tex} \footnotesize Rasterisation};
		
		\node (color) [fig, below of=data, xshift=1.5cm, yshift=-3cm,draw=blue!30,thick, dash pattern=on 2pt off 2pt] {\def \svgwidth{2cm}\input{svg/colour.pdf_tex} \footnotesize Fragment Shading};
		
		\node (depth) [fig, right of=color, xshift=2cm,draw=green!30] {\def \svgwidth{2cm}\input{svg/depth.pdf_tex} \footnotesize Depth Test};
		
		\node (frame) [fig, right of=depth, xshift=2cm,draw=black] {\def \svgwidth{2cm}\input{svg/frame_buffer.pdf_tex} \footnotesize Frame Buffer};		
		
		
		\path [line] (data) -- (trans);
		\path [line] (trans) -- (prim);	
		\path [line] (prim) -- (raster);
		\path [line] (raster) |- node [near start] {} ([yshift=-0.5cm] trans.south) -| (color.north);
		\path [line] (color) -- (depth);	
		\path [line] (depth) -- (frame);	
	\end{tikzpicture}
\end{frame}


\section{GLSL Shader Language}
% FRAME 4 GLSL
\begin{frame}[fragile]
\frametitle{GLSL: OpenGL Shading Language}
\begin{itemize}
\item<1-> To make changes in the rendering pipeline, early versions of OpenGL required use of the assembly language code
\item<1-> GLSL part of core functionality since OpenGL 2.0
\item<1-> Versions (from 3.3+) match OpenGL, e.g.
\begin{itemize}
	\item OpenGL 3.3 $\rightarrow$ GLSL 3.30: \lstinline{"#version 330"}
	\item OpenGL 4.5 $\rightarrow$ GLSL 4.50: \lstinline{"#version 450"}
\end{itemize}
\item<2-> C-like language, with looping, conditionals, and embedded math
\item<2-> Sent to graphics drivers as a string, compiled and linked during runtime
\item<2-> Vertex and Fragment shader programs most important
\item<2-> Tessellation, Geometry and Compute shaders also exist
\end{itemize}

\end{frame}

% FRAME 5 Data Types
\begin{frame}[fragile]
\frametitle{Shader Language Basics}

\lstGLSL
\begin{minipage}[t][1cm][c]{\textwidth}
\only<1>{A shader program requires a version and a \lstinline{void main()} function.\\The \lstinline{core} profile of GLSL is used by default. It could alternatively be set to \lstinline{compatibility}.}
\only<2>{Input variables are defined with the \lstinline{in} keyword and given a type.}
\only<3>{Likewise, outputs are defined with \lstinline{out}. This program will return a vector of length 3.}
\only<4>{Local variables can also be declared.\\General types include \lstinline{int}, \lstinline{float}, etc. as well as vectors and matrices, e.g. \lstinline{vec3} or \lstinline{mat4} for a length 3 vector and 4x4 matrix respectively.}
\only<5>{Uniforms are global variables used to pass data from memory to the shader program. They are unchanged within a single rendering call.}
\end{minipage}
\begin{onlyenv}<1>
\begin{lstlisting}
#version 330 core






void main()
{


}
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<2>
\begin{lstlisting}
#version 330 core

in vec3      input_variable;




void main()
{


}
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<3>
\begin{lstlisting}
#version 330 core

in vec3      input_variable;
out vec3     output_variable;



void main()
{
    // some code
    output_variable = ...
}

\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<4>
\begin{lstlisting}
#version 330 core

in vec3      input_variable;
out vec3     output_variable;
float        local_variable;


void main()
{
    // some code
    output_variable = ...
}
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<5>
\begin{lstlisting}
#version 330 core

in vec3      input_variable;
out vec3     output_variable;
float        local_variable;
uniform mat4 uniform_variable;

void main()
{
    // some code
    output_variable = ...
}
\end{lstlisting}
\end{onlyenv}


\end{frame}

\begin{frame}[fragile]
\frametitle{Vertex Shader}
A basic vertex shader takes in information from a set of vertex attributes. For each vertex, attribute information can include position $(x,y,z[,w])$, colour $(r,g,b[,a])$, and texture coordinates $(s,t)$. \\The following shader program takes in only the position information, and passes it to OpenGL via the \lstinline{gl_Position} variable.

\begin{lstlisting}
#version 330 core

layout (location = 0) in vec3 position;

void main()
{
    gl_Position = vec4(position, 1.0f);
}
\end{lstlisting}
\end{frame}

% FRAME 6 GLM
\begin{frame}[fragile]
\frametitle{Passing Data into the Shader}
\begin{minipage}[t][3.5cm][c]{\textwidth}
\lstCPP

\begin{onlyenv}<1>
\begin{itemize}
\item No direct data structures that match the special vector and matrix data types in GLSL
\item Pass vertex data into the shader program through vertex buffer objects
\end{itemize}
\end{onlyenv}

\begin{onlyenv}<2>
\begin{lstlisting}
GLfloat vertices[] =
{
//       position           colour
//    x     y     z     r     g     b
    -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // vertex 0
     1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // vertex 1
     0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f  // vertex 2
};
\end{lstlisting}
\end{onlyenv}
\begin{onlyenv}<3>
\lstset{basicstyle=\tiny}
\begin{lstlisting}
GLuint VBO; glGenBuffers(1, &VBO);    // Generate and 
glBindBuffer(GL_ARRAY_BUFFER, VBO);   //    bind vertex buffer object
glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
glVertexAttribPointer( // position data
// id count type                 stride            offset
    0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), (GLvoid*)0);
glEnableVertexAttribArray(0);
glVertexAttribPointer( // colour data
// id count type                 stride            offset
    1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), (GLvoid*)3*sizeof(GLfloat));
glEnableVertexAttribArray(1);
\end{lstlisting}
\end{onlyenv}


\end{minipage}
\begin{figure}[b]
{\def\svgwidth{0.9\textwidth}\input{svg/vbo.pdf_tex}}
\end{figure}

\end{frame}

%
\begin{frame}[fragile]
\frametitle{Getting Data in the Vertex Shader}
\lstGLSL
\begin{itemize}
\item The \lstinline{layout (location = ?)} declaration describes the assigned attribute location for the current vertex.
\item This is defined in the first parameter of \lstinline{glVertexAttribPointer()}.
\item Here, we pass the vertex colour as output to the \emph{Fragment Shader}.
\end{itemize}
\vspace{-10pt}
\begin{lstlisting}
#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 colour;

out vec3 _colour;

void main()
{
    gl_Position = vec4(position, 1.0f);
    _colour = colour;
}
\end{lstlisting}

\end{frame}


\begin{frame}[fragile]
\frametitle{Fragment Shader}
\begin{onlyenv}<1>
\begin{itemize}
\item The fragment shader outputs the colour of the each fragment
\item It takes input variables directly from the vertex shader
\item Here, calculations for colour shading, texturing and lighting can be made.
\item The output is always the RGBA colour of the fragment \lstinline{vec4 color}
\end{itemize}
\end{onlyenv}
\begin{onlyenv}<2>
The below example simply takes in the predefined vertex colour and assigns it to the fragment.
\begin{lstlisting}
#version 330 core

in vec3 _colour;

out vec4 color;

void main()
{
    color = vec4(_colour, 1.0f);
}
\end{lstlisting}
\end{onlyenv}
\end{frame}

\section{Modelling and Transformations}
% FRAME 8
\begin{frame}[fragile]
\frametitle{Drawing Objects}
\lstCPP
\begin{onlyenv}<1>
\begin{itemize}
\item To draw vertex buffer objects (VBO), they need to be attached to a vertex array object (VAO)
\item A VAO is effectively an array containing pointers to the vertex attributes
\item The VAO needs to be generated, and the OpenGL state bound to it
\item While the VAO is bound, all vertex attribute pointers are assigned to it.
\end{itemize}
\end{onlyenv}

\begin{onlyenv}<2>
\begin{lstlisting}
GLuint VAO; glGenVertexArrays(1, &VAO);
GLuint VBO; glGenBuffers(1, &VBO);
glBindVertexArray(VAO); // Bind VAO
{
    glBindBuffer(GL_ARRAY_BUFFER, VBO);   // Bind VBO
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices),
                 vertices, GL_STATIC_DRAW);
    // Position data (location = 0)
    glVertexAttribPointer( 0, 3, GL_FLOAT,
                           GL_FALSE, 6*sizeof(GLfloat),
                           (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Colour data (location = 1)
    glVertexAttribPointer( 1, 3, GL_FLOAT,
                           GL_FALSE, 6*sizeof(GLfloat),
                           (GLvoid*)3*sizeof(GLfloat));
    glEnableVertexAttribArray(1);
}
glBindVertexArray(NULL); // Unbind VAO
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<3>
During the render cycle, to draw the vertex array object, simply set the shader program, bind it and call the relevant draw function:
\begin{lstlisting}
glUseProgram(shaderProgram);
glBindVertexArray(VAO);
{
    glDrawArrays(GL_TRIANGLES, 0, 3);
}
glBindVertexArray(NULL);
\end{lstlisting}
\end{onlyenv}

\end{frame}


%%%%%%%%%%%%%%%%%%
% DEMO: Triangle %
%%%%%%%%%%%%%%%%%%

% FRAME 7
\begin{frame}[fragile]
\frametitle{Transformations}
\begin{itemize}
\item The model matrix is used to apply transformations to vertices
\end{itemize}
\begin{equation*}
\mathbf{v}' = \mathbf{M}\cdot\mathbf{v}
\end{equation*}
\begin{itemize}
\item GLM is a maths library for OpenGL to interface with GLSL
\item Includes data types such as \lstinline{glm::mat4} and \lstinline{glm::vec3}
\item Transformations and vector / matrix calculations
\end{itemize}

\lstCPP
\begin{lstlisting}
glm::mat4 model;
model = glm::translate(model, glm::vec3(1.0f,0.0f,0.0f));
\end{lstlisting}
\end{frame}

% Viewng and Projection
\begin{frame}[fragile]
\frametitle{Viewing and Projection}
\begin{itemize}
\item Alongside the model matrix, there are also transformations for both the view space and projection matrix
\end{itemize}
\begin{equation*}
\mathbf{v'} = \mathbf{P}\cdot\mathbf{V}\cdot\mathbf{M}\cdot\mathbf{v}
\end{equation*}
\begin{itemize}
\item Luckily, GLM provides calculations for these matrices (similar to GLUT)
\end{itemize}
\lstCPP
\begin{lstlisting}
glm::mat4 view, projection;
view = glm::lookat(glm::vec3(eye.x,eye.y,eye.z),
                   glm::vec3(cen.x,cen.y,cen.z),
                   glm::vec3(up.x, up.y, up.z));
projection = glm::perspective(60.0f,aspect,0.1f,1000.0f);
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Colour Cube}
\begin{onlyenv}<1>
\lstGLSL
\begin{lstlisting}
#version 330 core

layout (location = 0) in vec3 position;

out vec3 _colour;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    vec4 _position = vec4(position, 1.0f);
    gl_Position = projection * view * model * _position;
    _colour = position + 0.5f;
}
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<2>
\lstCPP
\lstset{basicstyle=\scriptsize}
\begin{lstlisting}
GLuint modelLoc, viewLoc, projLoc;

// Get location of uniform within shader program
modelLoc = glGetUniformLocation(shaderProgram,"model");
viewLoc = glGetUniformLocation(shaderProgram,"view");
prjLoc = glGetUniformLocation(shaderProgram,"projection");

// Assign 4x4 matrices to respective uniforms
glUniformMatrix4fv(modelLoc, 1, GL_FALSE,
                   glm::value_ptr(model));
glUniformMatrix4fv(viewLoc, 1, GL_FALSE,
                   glm::value_ptr(view));
glUniformMatrix4fv(prjLoc, 1, GL_FALSE,
                   glm::value_ptr(projection));
\end{lstlisting}
\end{onlyenv}

\end{frame}

\section{Basic Lighting}
\begin{frame}[fragile]
\frametitle{Material and Light Properties}

\only<1-2>{Fragment Shader}
\only<3>{C++ Program}
\begin{columns}[T]
	\begin{onlyenv}<1-2>
	
	\begin{column}[T]{5cm}
		\begin{lstlisting}
struct Light
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 position;
};
		\end{lstlisting}
	\end{column}
	\begin{uncoverenv}<2>
	\begin{column}[T]{5cm}
		\begin{lstlisting}
struct Material
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
};
		\end{lstlisting}
	\end{column}
	\end{uncoverenv}
	\end{onlyenv}
	
	\begin{onlyenv}<3>
	\lstCPP
	\begin{column}[T]{5cm}
		\begin{lstlisting}
struct Light
{
    glm::vec4 ambient;
    glm::vec4 diffuse;
    glm::vec4 specular;
    glm::vec4 position;
};		
		\end{lstlisting}
	\end{column}
	\begin{column}[T]{5cm}
		
		\begin{lstlisting}
struct Material
{
    glm::vec4 ambient;
    glm::vec4 diffuse;
    glm::vec4 specular;
    GLfloat shininess;
};		
		\end{lstlisting}
		\end{column}
		\end{onlyenv}
\end{columns}
\end{frame}

\begin{frame}[fragile]
\frametitle{Lighting}

\begin{itemize}
\item Ambient lighting is simplest, simply multiple light and material values
\end{itemize}\vspace{-0.5cm}
\begin{lstlisting}
vec4 ambient = light.ambient * mat.ambient;
\end{lstlisting}\pause
\begin{itemize}
\item<1-> Diffuse lighting is somewhat more complicated
\item<1-> The intensity of diffuse lighting is affected by the angle between normal and light direction:
\end{itemize}

\begin{equation*}
\cos\theta = \vec{n} \cdot \vec{l_d}
\end{equation*}

\end{frame}

\begin{frame}[fragile]
\frametitle{Lighting: Diffuse}
\begin{onlyenv}<1>
\begin{columns}[T]

	\begin{column}[T]{5cm}
		\begin{tikzpicture}	
			\path[line, draw=blue, thick] (0,0) -- (0,2) node [label=left:{$\color{blue}\vec{n}$}, text=blue] {};
			\draw[thick] (-2,0) -- (2,0);
			\draw (2,1.6) node [label=above:{$\color{red}\vec{l_d}$}, text=red] {};
			\path[line, draw=red, thick]  (2,1.6) -- (0,0);
			\draw[bend right, semithick,-] (0.3,0.24) to (0,0.4) node [label=above right:{$\theta$}]{};
		\end{tikzpicture} \\
		\begin{equation*}
		\cos\theta = \vec{n} \cdot \vec{l_d}
		\end{equation*}
	\end{column}
	\begin{column}[T]{5cm}
		\begin{tikzpicture}
			\begin{axis}[height=5cm, width=5cm, domain=-pi/2:pi/2, xlabel={$\theta$}, xtick={-90,-45,0,45,90}, ylabel=diffuse intensity]
			\addplot [red, thick] (deg(x),{cos(deg(x))});
			\end{axis}		
		\end{tikzpicture}
	\end{column}
\end{columns}
	\begin{lstlisting}
float diff_i = max(dot(norm, lightdir), 0.0f);
	\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<2>
\begin{lstlisting}
vec3 norm = normalize(_normal);
vec3 lightdir = normalize(light.position - _fragpos);

// Calculate diffuse intensity [0.0 - 1.0]
float diff_i = max(dot(norm, lightdir), 0.0f);

// Calculate diffuse colour
vec4 diffuse = diff_i * light.diffuse * mat.diffuse;

\end{lstlisting}
\end{onlyenv}

\end{frame}

\begin{frame}[fragile]
\frametitle{Lighting: Specular}

\begin{onlyenv}<1>
\begin{columns}[T]

	\begin{column}[T]{5cm}
		\begin{tikzpicture}	
			\path[line, draw=blue, thick] (0,0) -- (0,2) node [label=left:{$\color{blue}\vec{n}$}, text=blue] {};
			\draw[thick] (-2,0) -- (2,0);
			\draw (2,1.6) node [label=above:{$\color{red}\vec{l_d}$}, text=red] {};
			\draw (-2,1.6) node [label=above:{$\color{red}\vec{r_d}$}, text=red] {};
			\path[line, draw=red, thick]  (2,1.6) -- (0,0);
			\draw[bend right, semithick,-] (0.3,0.24) to (0,0.4);
			\path[line, draw=red, dash pattern=on 2pt off 2pt, thick] (0,0) -- (-2,1.6);
			\draw[bend left, semithick, -] (-0.3,0.24) to (0,0.4);
	
			\path[line, draw=green, thick] (-1,3) -- (0,0);
			\draw (-1,3) node [label=right:{$\color{green}\vec{v_d}$}]{};
			\draw[bend left, semithick, -] (-0.6,0.48) to (-0.25,0.75) node [label=left:{$\varphi$}]{};
					
		\end{tikzpicture} \\
		\begin{equation*}
		\cos\varphi = \vec{v_d} \cdot \vec{r_d}
		\end{equation*}
	\end{column}
	\begin{column}[T]{5cm}
		\begin{tikzpicture}
			\begin{axis}[height=5cm, width=5cm, domain=-pi/2:pi/2, xlabel={$\varphi$}, xtick={-90,-45,0,45,90}, ylabel=specular intensity]
			\addplot [green, thick] (deg(x),{cos(deg(x))});
			\end{axis}		
		\end{tikzpicture}
	\end{column}
\end{columns}
	\begin{lstlisting}
float diff_s = max(dot(lightdir, reflectdir), 0.0f);
	\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<2>
\begin{columns}[T]

	\begin{column}[T]{5cm}
		\begin{itemize}
		\item Material shininess $\alpha_m$ dictate size specular highlight.
		\item Higher $\alpha_m$ equals narrower highlight.
		\end{itemize}
		\centering
		\begin{tikzpicture}	
			\draw [draw=magenta, ultra thick] (-1,2) -- (-0.5,2) node [label=right:{\small $\alpha_m = 0.5$}]{};
			\draw [draw=green, ultra thick] (-1,1.5) -- (-0.5,1.5) node [label=right:{\small $\alpha_m = 1.0$}]{};
			\draw [draw=blue, ultra thick] (-1,1.0) -- (-0.5,1) node [label=right:{\small $\alpha_m = 2.0$}]{};
			\draw [draw=red, ultra thick] (-1,0.5) -- (-0.5,0.5) node [label=right:{\small $\alpha_m = 8.0$}]{};
			\draw [draw=cyan, ultra thick] (-1,0) -- (-0.5,0) node [label=right:{\small $\alpha_m = 32.0$}]{};
		\end{tikzpicture}
	\end{column}
	\begin{column}[T]{5cm}
	\vspace{1cm}
		\begin{tikzpicture}
			\begin{axis}[height=5cm, width=5cm, domain=-pi/2:pi/2, xlabel={$\varphi$}, xtick={-90,-45,0,45,90}, ylabel=specular intensity]
			\addplot [magenta, thick] (deg(x), {cos(deg(x))^0.5});
			\addplot [green, thick] (deg(x),{cos(deg(x))});
			\addplot [blue, thick] (deg(x), {cos(deg(x))^2});
			\addplot [red, thick] (deg(x), {cos(deg(x))^8});
			\addplot [cyan, thick] (deg(x), {cos(deg(x))^32});
			\end{axis}
		\end{tikzpicture}
	\end{column}
\end{columns}
	\begin{lstlisting}
float spec_i = max(dot(lightdir, reflectdir), 0.0f);
spec_i = pow(spec_i, mat.shininess);
	\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<3>
\begin{lstlisting}
vec3 viewdir = normalize(viewpos - _fragpos);
vec3 reflectdir = reflect(-lightdir, norm);

float spec_i = max(dot(viewdir, reflectdir), 0.0f);
spec_i  = pow(diff_s, mat.shininess);

vec4 specular = spec_i * light.specular * mat.specular;
\end{lstlisting}
\end{onlyenv}
\end{frame}

\begin{frame}[fragile]
\frametitle{Lighting: Phong Reflection}
\lstset{basicstyle=\tiny}
\vspace{-0.75cm}
\begin{columns}[T]
\begin{column}[T]{3.2cm}
\begin{lstlisting}
#version 330

struct Material
{
  vec4 ambient;
  vec4 diffuse;
  vec4 specular;
  float shininess;
};

struct Light
{
  vec4 ambient;
  vec4 diffuse;
  vec4 specular;
  vec4 position;
};

in vec3 _fragpos;
in vec3 _normal;

out vec4 color;

uniform vec3 viewpos;
uniform Material mat;
uniform Light light;
\end{lstlisting}
\end{column}
\begin{column}[T]{8cm}
\begin{onlyenv}<1>
\begin{lstlisting}
main()
{
/////// AMBIENT /////////////////////////////////////////
// Calculate ambient colour
  vec4 ambient = light.ambient * mat.ambient;

















/////// Phong Reflection ////////////////////////////////
// Combine lighting elements
  color = ambient;
}
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<2>
\begin{lstlisting}
main()
{
/////// AMBIENT /////////////////////////////////////////
// Calculate ambient colour
  vec4 ambient = light.ambient * mat.ambient;
/////// DIFFUSE /////////////////////////////////////////
// Normalise normal and light direction
  vec3 norm = normalize(_normal);
  vec3 lightdir = normalize(light.position - _fragpos);
// Calculate diffuse intensity
  float diff_i = max(dot(norm, lightdir), 0.0f);
// Calculate diffuse colour
  vec4 diffuse = diff_i * light.diffuse * mat.diffuse;









/////// Phong Reflection ////////////////////////////////
// Combine lighting elements
  color = ambient + diffuse;
}
\end{lstlisting}
\end{onlyenv}

\begin{onlyenv}<3>
\begin{lstlisting}
main()
{
/////// AMBIENT /////////////////////////////////////////
// Calculate ambient colour
  vec4 ambient = light.ambient * mat.ambient;
/////// DIFFUSE /////////////////////////////////////////
// Normalise normal and light direction
  vec3 norm = normalize(_normal);
  vec3 lightdir = normalize(light.position - _fragpos);
// Calculate diffuse intensity
  float diff_i = max(dot(norm, lightdir), 0.0f);
// Calculate diffuse colour
  vec4 diffuse = diff_i * light.diffuse * mat.diffuse;
/////// SPECULAR ////////////////////////////////////////
// Normalise reflection and view direction
  vec3 viewdir = normalize(viewpos - _fragpos);
  vec3 reflectdir = reflect(-lightdir, norm);
// Calculate specular intensity
  float spec_i = max(dot(viewdir, reflectdir), 0.0f);
  spec_i = pow(diff_s, mat.shininess);
// Calculate specular colour
  vec4 specular = spec_i * light.specular * mat.specular;
/////// Phong Reflection ////////////////////////////////
// Combine lighting elements
  color = ambient + diffuse + specular;
}
\end{lstlisting}
\end{onlyenv}
\end{column}
\end{columns}

\end{frame}

\begin{frame}[fragile]
\frametitle{Lighting: Vertex Shader}
\lstset{basicstyle=\tiny}
\begin{lstlisting}
#version 330

layout (location=0) in vec3 position;
layout (location=1) in vec3 normal;

out vec3 _fragpos;
out vec3 _normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    vec4 _position = vec4(position, 1.0f);
    gl_Position = projection * view * model * _position;
    
    _fragpos = vec3(model * _position);   
    _normal  = mat3(transpose(inverse(model))) * normal;
}
\end{lstlisting}


\end{frame}

\section*{}
\begin{frame}
\frametitle{Resources}
\begin{description}
\item[\emph{GLFWFramework}] Framework built for Shader programming\\
{\footnotesize \color{blue}
\url{projects.cs.nott.ac.uk/psxwowa/GLFWFramework/}
}
\item[\emph{LearnOpenGL}] Tutorials covering modern OpenGL from setting up to advanced lighting\\
{\footnotesize \color{blue}
\url{learnopengl.com}
}
\item[OpenGL.org] Full documentation and releases by Khronos\\
{\footnotesize \color{blue}
\url{www.opengl.org/documentation/}
}
\item[GLFW] An OpenGL library for windowing and event handling\\
{\footnotesize \color{blue}
\url{www.glfw.org/}
}
\item[GLM] OpenGL Mathematics, a header only C++ library\\
{\footnotesize \color{blue}
\url{github.com/g-truc/glm}
}
\end{description}

\end{frame}

\end{document}